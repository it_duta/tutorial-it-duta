Selamat datang di dunia *Competitive Programming*!
===

Afiliasi IT divisi programming dan tim olimpiade sains cabang informatika, SMAN 2 Kota Tangerang!

Note : Semua materi tentang *Competitive Programming* akan di upload disini sewaktu - waktu, stay tooned!

## GO GET GOLD!

## Apakah itu olimpiade sains cabang informatika?

Olimpiade sains cabang informatika adalah olimpiade yang memperlombakan tentang bagaimana cara kita berfikir, menemukan konsep dari suatu masalah, merumuskan masalah, hingga memecahkan masalah dengan menyusun solusi - solusi yang dianggap paling baik dan paling efisien sesuai dengan kaidah logika yang baik dan benar

Disini, kamu tidak hanya diajarkan sekedar bisa membuat program. Namun bagaimana supaya program tersebut bisa bekerja secara seefisien mungkin. Dan mengasah pola pikir kamu dalam kehidupan sehari - hari.

## Hah? emangnya seperti apa sih?
Misalkan saja saya ingin memasang ubin di dasar ruangan, asumsikan aja ada ubin berbentuk L 2x2 satuan dan ubin berukuran 1x1. Jika diminta menghitung berapa banyak variasi pemasangannya dengan ubin tersebut tanpa ada yang boleh dipotong namun boleh di putar, ada berapa hayo?

Mungkin kalo pake cara paradigma pemrograman biasa, ya cobain aja satu - satu, ya kalo ukuran dasar lantai yang mau dipasang ubin cuma 2x2 satuan, kalo misalkan ukuran yg mau dipasang ada 100 x 2 satuan?

Atau misalkan saja kita punya empat tiang bendera mau kita pasang oleh bendera dengan tiga jenis warna. Sebut saja merah, kuning, hijau. Jika tidak boleh ada dua bendera bersebelahan yang warnanya bersamaan, ada berapa banyak variasinya? (Soal UN MTK SMA 2017)

Atau bahkan sesederhana ini : Kalian dari duta pulang kemana? Misalkan saja rumah kalian di Poris. Jalan mana yang akan kalian tempuh untuk pulang? Well, bisa aja kalian memilih naik kereta jalan - jalan dulu ke Bogor, enggak tap out lalu balik ke poris :D. Tapi kalo diminta harus sampai 30 menit lagi dan kamu hanya punya uang 5000 sedangkan hp kalian mati dan teman kalian udah pada pulang. Apa yang akan kamu lakukan?

## Wah, serumit itu ya? Tapi mengapa saya harus belajar seputar olimpiade informatika?
Tentu saja olimpiade informatika sangat dianjurkan untuk dielajari *bahkan andaikan pun kalian tidak terjun di bidang informatika*. Mengapa? Menambah ilmu yang bermanfaat serta mengasah problem solving kalian. Soal - soal matdas SBM dan tpa banyak yang mirip seperti soal informatika : Peluang, permutasi kombinasi, sebab akibat, tata urutan melakukan sesuatu, dan masih banyak lagi.

Pada tahap ini, kalian akan dibimbing oleh kakak - kakak kalian yang sudah berpengalaman untuk mengikuti olimpiade sains cabang informatika (OSN) dan sejenisnya. Lumayan kan selain ilmu yang bermanfaat juga kalian dapat berkontribusi terhadap sekolah *(dan mendapatkan sejuta surat dispensasi serta meningkatkan derajat diri sebagai anak olim. Syukur syukur kalo menang bisa nginep di hotel mewah gratis hehehe)*

## Wah, kalo mau belajar dan join bagaimana?
Untuk sementara, kalian ikuti semua tutorial dan materi tentang programming biasa dulu ya. Setelah itu nanti kurang lebih setelah Siswa Baru masuk kami akan mengadakan semacam seleksi untuk kalian yang ingin bergabung bersama kami. Ya kelas 12 diperbolehkan ikut namun hanya sebatas belajar saja enggak bisa sampai ikut rangkaian OSN ya (:

Diharapkan, siswa - siswa terpilih seperti kalian akan menjadi yang terbaik yang dapat mewakili duta ke nasional. Bahkan mewakili Indonesia ke ajang IOI dan ACM ICPC (setelah kuliah)

Untuk spoiler tentang materi dan lainnya, bisa cekidot di laman resmi TOKI (Tim Olimpiade Komputer Indonesia) di [sini!](https://toki.id/downloads/)

GO GET GOLD!

***
2019 (C) SIG Competitive Programming Doeta

Author : Muhamad Nicky Salim