Challenge 0: Rekap nilai!
===

IT SMAN 2 Kota Tangerang, divisi Programming

***

Pada challenge kali ini, kalian akan mulai ditantang untuk membuat sebuah program sederhana
untuk menghitung nilai rapor. Untuk tutorial kali ini, kalian akan belajar menggunakan input/output,
menggunakan dan menghitung variabel, dan membuat kondisional.

Jika kalian butuh bantuan, bacalah [petunjuk](#petunjuk) di bawah, atau tanyakan kepada kakak
pembina kalian saat pertemuan!

## Skenario

Pelajaran kesukaan Nyonyo' adalah Bahasa Perancis. Karena dia benar-benar mencintai mata pelajaran ini,
dia menargetkan nilai A untuk pelajaran ini. Karena itu, dia ingin mencatat semua nilai yang dia
peroleh, sehingga Nyonyo' dapat dengan mudah memprediksi nilai akhirnya. Tapi sayangnya, Noynyo' malas
berhitung, sehingga dia meminta kepada kalian untuk membuatkan sebuah program dasar untuk menghitung
semua nilai yang didapatkannya.

## Spesifikasi

1. Nilai yang dimasukkan ada 4 buah ulangan, UTS dan UAS. Nilai berkisar dari 0 sampai 100
dan harus berupa bilangan bulat.
2. Program meminta kepada user berturut-turut nilai ulangan 1, ulangan 2, UTS,
ulangan 3, ulangan 4, UAS.
3. Untuk nilai akhir, rumus yang digunakan adalah
`(ulangan1 + ulangan2 + ulangan3 + ulangan4 + 2*uts + 2*uas)/8`.
4. Untuk nilai huruf akhir, gunakan tabel berikut.

| Nilai Akhir | Nilai huruf |
| - | - |
| 0 ≤ n < 50 | F |
| 50 ≤ n < 60 | D |
| 60 ≤ n < 70 | C |
| 70 ≤ n < 85 | B |
| 85 ≤ n ≤ 100 | A |

5. Output nilai angka dan nilai huruf tersebut kepada user.

## Contoh

Dialog interaksi dengan user tidak harus persis sama seperti contoh.
```
Masukkan nilai ulangan pertama.
70
Masukkan nilai ulangan kedua.
89
Masukkan nilai ulangan tengah semester.
97
Masukkan nilai ulangan ketiga.
50
Masukkan nilai ulangan keempat.
80
Masukkan nilai ulangan akhir semester.
100
---
Nilai akhir anda adalah 85.375. Anda mendapatkan nilai A!
```

## Petunjuk

Berikut adalah beberapa fungsi yang bisa kalian gunakan.

```py
print("hello!")
```
Kode ini akan mencetak `hello!` ke layar.

```py
var = input()
```
Kode ini akan meminta input dari user, lalu disimpan ke variabel `var` **berupa string**.

```py
int_var = int(string_var)
```
Kode ini akan mengubah `string_var` menjadi integer, lalu disimpan ke `int_var`.

```py
if x < 50:
    # do code
```
Potongan kode ini akan mengecek apakah variabel `x` lebih kecil dari pada 50. Bila iya, maka `# do code` akan
dijalankan. Jika tidak, maka `# do code` tidak akan dijalankan.

```py
if x < 50:
    # do code_1
elif x < 70:
    # do code_2
else:
    # do code_3
```
Potongan kode ini akan mengecek apakah variabel `x` lebih kecil dari pada 50. Bila iya, maka `# do code_1` akan
dijalankan. Jika tidak, maka akan dicek apakah `x` < 70. Bila iya, maka `# do code_2` akan dijalankan. Bila
tidak, maka hanya `# do code_3` yang akan dijalankan.


_**-- Selamat mengerjakan dan semoga bersenang senang! --**_

***

Copyright (c) 2019, Aloysius Kurnia Mahendra

This file is free to copy and modify.
