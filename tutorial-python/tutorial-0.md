Tutorial 0: Install Python
===

IT SMAN 2 Kota Tangerang, divisi Programming

***

Dokumen ini mengandung tutorial bagaimana menginstall Python 3.6.7. Setelah
mengikuti tutorial ini, kalian diharapkan dapat menginstall Python 3.6.7 sendiri
di dalam komputer/laptop kalian masing-masing. Selamat ngoding!

## Menginstall Python 3.6.7

Pastikan kalian sudah tidak memakai Windows XP!

1. Buka [website dowload Python 3.6.7](https://www.python.org/downloads/release/python-367/)
2. Scroll ke bawah, cari bagian *files*. Di sini akan tersedia beberapa pilihan download.  
![Bagian files.](img/1-1.PNG "Bagian files.")
3. Pilih "Windows x86-64 executable installer" jika komputer kalian 64-bit, atau
"Windows x86 executable installer" jika komputer kalian 32-bit.
4. File yang kalian pilih akan diunduh.
5. Jalankan installer Python tersebut.
6. **Penting**: Klik "Add Python 3.6 to PATH" di bagian bawah installer. Ini dilakukan supaya
mengakses Python lebih mudah dilakukan bila menggunakan command line.
7. Klik "Install now".  
![Jangan lupa 'Add Python 3.6 to PATH'!](img/1-2.PNG "Jangan lupa 'Add Python 3.6 to PATH'!")
8. Tunggu sampai instalasi selesai.
9. Daaaan selesai! Kalian sudah bisa menggunakan Python 3.7.6!

## Mulai Bermain dengan Python 3.6.7

1. Buka Windows search bar, ketikkan "IDLE".
2. Pilih **IDLE (Python 3.6)**.  
![Opening Python...](img/1-3.PNG "Opening Python...")
3. Kalian akan dihadapkan dengan command line bawaan Python. Dari menu, klik File -> New File
(atau tekan ctrl + N).
4. Akan tampil sebuah text editor polos.
5. Copy-paste potongan kode berikut.  
```python
print("Halo dunia!")
nama = input("Masukkan namamu! > ")

if nama == "":
	print("Dih, pelit, nama aja ga boleh tahu!")
	nama = "tanpa nama"

makanan_kesukaan = input("Apa makanan kesukaanmu? > ")

print("Halo, " + nama + "!")
if makanan_kesukaan == "":
	print("Tadinya aku mau menyiapkan makanan kesukaanmu buat kamu"
		+ ", tapi ku ga tahu kau suka apa jadi yaaah...")
else:
	print("Aku sudah menyiapkan "
		+ makanan_kesukaan
		+ " di atas meja. Selamat menikmati!")
```

6. Simpan kode tersebut dengan menu atau ctrl + S. Beri nama file sesuka kalian.
7. Jalankan kode dengan memilih Run -> Run Module dari menu. Dapat juga menggunakan F5.

***

Copyright (c) 2019, Aloysius Kurnia Mahendra

This file is free to copy and modify.
