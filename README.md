Repository Tutorial dan File untuk IT Doeta divisi Programming
===

IT SMAN 2 Kota Tangerang, divisi Programming

***

*Salvete, omnes!* Selamat datang di repository Programming Doeta!

Jadi, repository ini akan berisi tutorial untuk pelajaran pemrograman di IT doeta. Tutorial akan di-*update* sewaktu-waktu, dan update yang penting akan diumumkan ke group LINE, jadi stay tuned di group LINE yah!

## Tutorials
[Tutorial 0: Menginstall Python](tutorial-python/tutorial-0.md)

## Challenges
[Challenge 0: Rekap nilai](tutorial-python/challenge-0.md)

***
2019 (C) PseudoLW